# Universal Celestial Calendar App

A Ubuntu Touch webapp to display the Universal Celestial Calendar (see: https://ucc.zone/)

[![OpenStore](https://open-store.io/badges/en_US.png)](https://open-store.io/app/calclock-ut.prajnapranab)
